import os
from flask import Flask, request, jsonify, g, render_template, redirect, url_for
import sqlite3
from datetime import datetime
import random

from csm import *

DATABASE = "./scm.db"

app = Flask(__name__)

app.config["DEBUG"] = True
id = 0
user_data = []
isLoggedIn = True


##home page
@app.route("/", methods=["POST", "GET"])
def home():
    global user_data
    global isLoggedIn
    if isLoggedIn:
        return redirect(url_for("dashboard"))
    else:
        return redirect(url_for("login"))


##login page
@app.route("/login", methods=["POST", "GET"])
def login():
    global user_data
    global isLoggedIn

    if request.method == "POST":
        print("lg received")
        res = index("user")
        for i in res:
            if i[1] == request.form["username"] and i[2] == request.form["password"]:
                print("loggedin")
                isLoggedIn = True
                break
        if isLoggedIn:
            user_data = i
            return redirect(url_for("home"))
    return render_template("login.html")


##new issue page
@app.route("/new_issue", methods=["POST", "GET"])
def newIssue():
    global isLoggedIn
    db = get_db()
    if not isLoggedIn:
        return redirect(url_for("login"))
    if request.method == "POST":
        try:
            orderID = request.form["orderID"]
            productID = request.form["productID"]
            quantity = request.form["quantity"]
            supplierID = request.form["supplierID"]
            reason = request.form["reason"]
            now = datetime.now()
            priority = request.form["priority"]

            rand = random.randrange(1, 5, 1)
            predictedAction = predict([reason, priority, rand])

            for i in predictedAction:
                k = i

            cur = get_db().cursor()
            cur.execute(
                "INSERT INTO Issues (orderID,productID,quantity,supplierID,rootCause,timestamp,priority,status,action) VALUES (?,?,?,?,?,?,?,?,?)",
                (orderID, productID, quantity, supplierID, reason, now, priority, 0, k),
            )

            db.commit()
            msg = "Record successfully added"
        except Exception as e:
            db.rollback()
            msg = "error in insert operation"
            print(e)
        finally:
            print(msg)
            db.close()
            return redirect(url_for("dashboard"))
    return render_template("home.html", selected_page="new_issue")


##track issues page
@app.route("/track_issues", methods=["POST", "GET"])
def trackIssuesPage():
    global isLoggedIn
    if not isLoggedIn:
        return redirect(url_for("login"))
    cur = get_db().cursor()
    res = cur.execute("select * from Issues order by timestamp desc")
    return render_template("home.html", selected_page="track_issues", issueList=res)


##dashboard
@app.route("/dashboard", methods=["GET"])
def dashboard():
    global isLoggedIn
    if not isLoggedIn:
        return redirect(url_for("login"))
    cur = get_db().cursor()
    res = cur.execute("select * from Issues order by timestamp desc")
    lst = list()
    count = 0
    for i in res:
        if i[7] == 1:
            continue
        count += 1
        lst.append(i)
        if count == 3:
            break
    print(lst)

    return render_template(
        "home.html", selected_page="dashboard", recentList=lst, length=range(len(lst))
    )


##pie chart on dashboard
@app.route("/pie", methods=["GET"])
def pieChart():
    global isLoggedIn
    if not isLoggedIn:
        return redirect(url_for("login"))
    cur = get_db().cursor()
    res = cur.execute("select * from Issues order by timestamp")
    sol = 0
    uns = 0
    for i in res:
        if i[7] == 0:
            uns += 1
        else:

            sol += 1
    total = sol + uns
    try:

        sol = sol / total * 100
        uns = uns / total * 100
    except:
        sol = 0
        uns = 0
    return str(sol)


##solve button handler
@app.route("/take-action", methods=["POST", "GET"])
def takeAction():
    if request.method == "POST":
        orderID = request.form["orderID"]

        db = get_db()
        cur = db.cursor()
        cur.execute("update Issues set status=1 where orderID=" + orderID)
        db.commit()
        print("success")

    return redirect(url_for("trackIssuesPage"))


def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


def index(table):
    cur = get_db().cursor()
    res = cur.execute("select * from " + table)
    return res


if __name__ == "__main__":
    app.run()
