$(document).ready(function() {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open('GET', '/pie', false); // false for synchronous request
	xmlHttp.send(null);
	console.log('started');
	console.log(xmlHttp.responseText.toString());
	var data = [
		{
			values: [ parseFloat(xmlHttp.responseText), 100 - parseFloat(xmlHttp.responseText) ],
			labels: [ 'Solved', 'Unsolved' ],
			type: 'pie',
			text: [ 'Solved', 'Unsolved' ]
		}
	];
	var layout = {
		autoexpand: 'true',
		autosize: 'true',
		margin: {
			autoexpand: 'true',
			margon: 0
		},
		title: 'Solved - Unsolved Pie Chart'
	};

	var config = {
		responsive: 'true',
		displaylogo: 'false'
	};

	Plotly.newPlot('pieChart', data, layout, config);
});
