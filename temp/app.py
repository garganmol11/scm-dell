import os
from flask import Flask, request, jsonify ,g, render_template ,redirect , url_for
from flask_sqlalchemy import SQLAlchemy
import sqlite3


DATABASE = "./smc.db"


app = Flask(__name__)



app.config['APP_SETTINGS']='config.DevelopmentConfig'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///dell'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
app.config['DEBUG'] = True
id=0
user_data=[]


@app.route("/home" , methods=['GET','POST'])
def home():
    global user_data
    print("HII")
    print(user_data)
    specs=['1 GB ' , '256 GB ' , ' Windows Os']
    if request.method == 'POST':
    	#print("POST METHOD CALLED")
    	print(request.form['orderid'])
    	print(request.form['productid'])
    else:
        print("Get method called")
    return render_template('home.html',users=user_data[1:],specs=specs)

@app.route("/", methods=['GET', 'POST'])
def login():
    global user_data
    error = None
    flag=0
    if request.method == 'POST':
        res = index()
        print(res)
        for i in res:
            print(i[1] + i[2])
            if(i[1]==request.form['username'] and i[2]==request.form['password']):
                flag=1
                break
        if(flag==1):
            user_data=[i[0],i[1],i[2],i[3],i[4],i[5],i[6]]
            id=i[0]
            print(user_data)
            return redirect(url_for('home'))
            
    return render_template('index.html')
    
@app.route("/OptimiseNetwork.html", methods=['GET', 'POST'])
def optimise():
    return render_template('OptimiseNetwork.html')

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

def index():
    cur = get_db().cursor()
    res = cur.execute("select * from user")
    return res

if __name__ == '__main__':
    app.run()
